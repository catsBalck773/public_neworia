import {
  Controller,
  Get,
  Query,
  Header,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';

import { ArticlesService } from './articles.service';

import { IResponse } from "src/interface/IResponse";
import { getResponseSuccess } from 'src/utils/getResponse';

@Controller('articles')
export class ArticlesController {
  constructor(
    private readonly articlesService: ArticlesService
  ) { }

  @Get()
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async find(@Query('limit') limit: number, @Query('offset') offset: number): Promise<IResponse> {
    const articles = await this.articlesService.findAll(limit, offset);
    return getResponseSuccess({ articles });
  }
}
