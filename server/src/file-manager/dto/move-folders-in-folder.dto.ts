import { IsArray, IsString } from "class-validator";

export class MoveFoldersInFolderDto {
  @IsArray()
  folders: string[];

  @IsString()
  folder_id: string;
}
