import { Column, CreateDateColumn, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { ArticleEntity } from "src/articles/entities/article.entity";

@Entity({ name: 'articles_images' })
export class ImageEntity {
  @PrimaryGeneratedColumn({ type: 'bigint', name: 'id' })
  id: number;

  @Column({ type: 'varchar', name: 'path', length: 255 })
  path: string;

  @Column({ type: 'varchar', name: 'name', length: 99 })
  name: string;

  @ManyToMany(() => ArticleEntity)
  @JoinTable({ name: 'articles_images_union' })
  articles: ArticleEntity[];

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'update_at' })
  updatedAt: Date;
}
