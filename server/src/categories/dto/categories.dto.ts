import { IsNotEmpty, IsString, MinLength } from "class-validator";

export class CategoriesDto {
  @IsNotEmpty()
  @IsString()
  @MinLength(3, { message: 'Минимальное количество символов 3' })
  readonly name: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(3, { message: 'Минимальное количество символов 3' })
  readonly title: string;
}
