import axios from "axios";
import { useCookies } from "vue3-cookies";

const { cookies } = useCookies();
const isProd = false;
const headers = {
  'x-neworia': true,
  'Authorization': null,
}

const authToken = cookies.get('auth_token');
if (authToken) {
  headers.Authorization = `Bearer ${authToken}`
}

export default axios.create({
  baseURL: isProd ? 'http://api.neworia.ru' : 'http://localhost:3030',
  headers: headers,
});
