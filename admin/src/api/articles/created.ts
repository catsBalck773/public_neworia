import HTTP from "@/api/HTTP";
import { ArticleResponse } from "@/types/articles/article.response.type";
import { HeadersType } from "@/types/headers.type";

export default async (formData: FormData, headers: HeadersType = {}): Promise<ArticleResponse> =>
  HTTP.post('/article/', formData, {
    headers
  });
