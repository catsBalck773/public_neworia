import HTTP from "../HTTP";
import { HeadersType } from "@/types/headers.type";
import { AuthResponseType } from "@/types/auth/auth.response.type";

export default async (headers: HeadersType = {}): Promise<AuthResponseType> =>
  HTTP.post('/auth/check', {}, {
    headers
  })
