export type HeaderType = string | number | boolean | null;

export interface HeadersType {
  [key: string]: HeaderType;
}
