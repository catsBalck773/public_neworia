import { CanActivate, ExecutionContext, Injectable, NotFoundException } from "@nestjs/common";
import { ArticleService } from "src/article/article.service";
import { CategoriesService } from "src/categories/categories.service";
import { UserService } from "src/user/user.service";


@Injectable()
export class AuthorGuard implements CanActivate {
  constructor(
    private readonly articleService: ArticleService,
    private readonly categoriesService: CategoriesService,
    private readonly userService: UserService
  ) { }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();

    const user = request.user;
    const { id, type } = request.params;

    let entity: any;
    switch (type) {
      case 'article':
        entity = await this.articleService.findOne(id);
        break;
      case 'category':
        entity = await this.categoriesService.findOne(id);
        break;
      case 'user':
        entity = await this.userService.findByLogin(user.login)
        break;
      default:
        throw new NotFoundException("Вы не являетесь автором");
    }

    return entity && user && (type === 'user' ? entity.id === user.id : entity.uid === user.id);
  }
}
