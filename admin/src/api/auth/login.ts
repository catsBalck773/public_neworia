import HTTP from "@/api/HTTP";
import { AuthLoginType } from "@/types/auth/auth.login.type";
import { AuthResponseType } from "@/types/auth/auth.response.type";
import { HeadersType } from "@/types/headers.type";

export default async (data: AuthLoginType, headers: HeadersType = {}): Promise<AuthResponseType> =>
  HTTP.post('/auth/login', data, headers)