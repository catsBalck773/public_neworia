import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import * as express from 'express';
import { join } from "path";
import helmet from 'helmet';

import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors({ credentials: true, origin: true });

  // TODO - почему-то думает что нахожусь в папке dist
  app.use('/uploads', express.static(join(__dirname, '..', '..', 'uploads')));

  app.use(helmet());

  app.use('/robots.txt', function (_, res: express.Response) {
    res.type('text/plain');
    res.send("User-agent: *\nDisallow: /");
  });

  await app.listen(3030);
}
bootstrap();
