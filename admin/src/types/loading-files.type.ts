import { LoadingPacksType } from "./loading-packs.type";

export interface LoadingFilesType extends LoadingPacksType {
  path: string;
}
