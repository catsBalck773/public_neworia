import { IsNotEmpty, MaxLength, MinLength, IsString, ArrayNotEmpty, IsArray, ArrayUnique } from "class-validator";
import { CategoryEntity } from "src/categories/entities/category.entity";
import { ImageEntity } from "src/images/entities/image.entity";

export class CreatedArticleDto {
  @IsNotEmpty()
  @MinLength(3)
  @MaxLength(255)
  @IsString()
  readonly title: string;

  @IsNotEmpty()
  @MinLength(3)
  @IsString()
  readonly description: string;

  @IsNotEmpty()
  @MinLength(3)
  @IsString()
  readonly content: string;

  @IsArray()
  @ArrayNotEmpty()
  @ArrayUnique()
  readonly images: ImageEntity[];

  @IsArray()
  @ArrayNotEmpty()
  @ArrayUnique()
  readonly categories: CategoryEntity[];
}
