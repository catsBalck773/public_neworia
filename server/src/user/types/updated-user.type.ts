export interface IUpdatedUser {
  id: string;
  login: string;
  email: string;
  group_id: number;
}
