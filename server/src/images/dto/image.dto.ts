export interface IImages {
  src: string,
  title: string,
}

export class ImagesDto {
  src: string;
  title: string;
}