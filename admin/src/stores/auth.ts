import { ref, computed } from "vue";
import { defineStore } from "pinia";
import { useCookies } from "vue3-cookies";

import { Auth as AuthApi } from "@/api";

import { AuthLoginType } from "@/types/auth/auth.login.type";
import { AuthType } from "@/types/auth/auth.type";
import { ResponseDataType } from "@/types/response.type";

const defaultAuthData: AuthType = {
  id: null,
  token: undefined,
  login: undefined,
  group_id: -1,
  time: new Date,
}

export const useAuthStore = defineStore('auth', () => {
  const { cookies } = useCookies();
  const isCheckAuth = ref<boolean>(false);
  const auth = ref<AuthType>(JSON.parse(JSON.stringify(defaultAuthData)));
  const isAuthLoad = ref<boolean>(false);
  const isAuth = computed(() => Boolean(auth.value.token));

  const setAuthData = (data: AuthType): void => {
    auth.value = data;
    cookies.set('auth_token', data.token);
    localStorage.setItem('user', JSON.stringify({
      id: data.id,
      login: data.login,
      group_id: data.group_id || 1,
    }));
  }

  const login = async (payload: AuthLoginType): Promise<ResponseDataType> => {
    isAuthLoad.value = true;
    const result = await AuthApi.login(payload)
      .then(response => {
        if (response.data.success) {
          setAuthData({
            ...response.data.data,
            time: new Date,
          });
        }
        return response.data;
      })
      .catch(err => ({
        success: false,
        data: [],
        messages: [err.message]
      }));

    isAuthLoad.value = false;
    return result;
  }

  const logout = async (login: string, token: string) => {
    // запрос на удаление токена

    // очистка данных
    cookies.remove('auth_token');
    auth.value = JSON.parse(JSON.stringify(defaultAuthData));
  }

  const authCheck = async (): Promise<ResponseDataType> => {
    const token = cookies.get('auth_token');
    if (!token) {
      isCheckAuth.value = true;
      return {
        success: false,
        data: { statusCode: 401 },
        messages: ['Error: not token']
      }
    }
    const userLocal: string = localStorage.getItem('user');
    const userData: AuthType = userLocal ? JSON.parse(userLocal) : undefined;
    if (!userData) {
      isCheckAuth.value = true;
      return {
        success: false,
        data: { statusCode: 401 },
        messages: ['Error: not userData']
      }
    }
    const login = userData.login;
    isAuthLoad.value = true;
    const result = await AuthApi.authCheck({
      token,
      login,
    })
      .then(response => {
        if (response.data.success) {
          setAuthData({
            ...response.data.data,
            token: token,
            time: new Date,
          });
        }
        return response.data;
      })
      .catch(({ response }) => {
        return  {
          success: false,
          data: response.data,
          messages: [response.data.message]
        }
      });

    isCheckAuth.value = true;
    isAuthLoad.value = false;
    return result;
  }

  return {
    isCheckAuth,
    isAuthLoad,
    auth,
    isAuth,
    login,
    logout,
    authCheck,
  }
});
