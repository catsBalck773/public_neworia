import HTTP from "@/api/HTTP";
import { LoadingPacksType } from "@/types/loading-packs.type";
import { ArticlesResponse } from "@/types/articles/article.response.type";

export default async (params: LoadingPacksType, headers: object = {}): Promise<ArticlesResponse> =>
  HTTP.get('/articles', {
    params,
    headers,
  });
