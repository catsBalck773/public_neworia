import getFiles from "./getFiles";
import getList from "./getList";
import uploadFile from "./uploadFile";
import getTypeFile from "./getTypeFile";
import createFolder from "./createFolder";
import moveFiles from "./moveFiles";
import moveFolders from "./moveFolders";

export {
  getFiles,
  getList,
  uploadFile,
  getTypeFile,
  createFolder,
  moveFiles,
  moveFolders,
}

export default {
  getFiles,
  getList,
  uploadFile,
  getTypeFile,
  createFolder,
  moveFiles,
  moveFolders,
}
