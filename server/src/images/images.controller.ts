import { Controller, Delete, FileTypeValidator, Header, HttpCode, HttpStatus, MaxFileSizeValidator, ParseFilePipe, Post, UploadedFile, UploadedFiles, UseGuards, UseInterceptors } from '@nestjs/common';
import { FileInterceptor, FilesInterceptor } from "@nestjs/platform-express";
import { diskStorage } from 'multer';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { AdminGuard } from 'src/guards/admin.guard';
import { IResponse } from 'src/interface/IResponse';
import { ImagesService } from './images.service';
import { getResponseSuccess } from 'src/utils/getResponse';

@Controller('images')
export class ImagesController {
  constructor(
    private readonly imageService: ImagesService
  ) { }

  @Post('upload/file')
  // @UseGuards(JwtAuthGuard, AdminGuard)
  @UseInterceptors(FileInterceptor('file', {
    storage: diskStorage({
      destination: './temps'
    })
  }))
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.CREATED)
  async uploadFile(@UploadedFile(
    new ParseFilePipe({
      validators: [
        new MaxFileSizeValidator({ maxSize: 524288 }),
        new FileTypeValidator({ fileType: /(image\/jpeg|image\/png|image\/gif)$/ }),
      ],
    }),
  ) file: Express.Multer.File): Promise<IResponse> {
    const result = await this.imageService.uploadImage(file);
    return getResponseSuccess(result);
  }

  @Delete('remove/file')
  @UseGuards(JwtAuthGuard, AdminGuard)
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async remove(): Promise<IResponse> {
    return getResponseSuccess([]);
  }
}
