export type moveFilesParams = {
  files: string[],
  folder_id: string,
}
