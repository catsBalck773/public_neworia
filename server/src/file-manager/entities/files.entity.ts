import { Entity, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, Column, ManyToOne, JoinColumn, ManyToMany } from "typeorm";
import { FilesTypeEntity } from "./files-type.entity";
import { FoldersEntity } from "./folders.entity";
import { UserEntity } from "src/users/entities/user.entity";

@Entity({ name: 'files' })
export class FilesEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar', name: 'file_name', length: 99 })
  fileName: string;

  @Column({ type: 'bigint' })
  size: number;

  @ManyToOne(() => FilesTypeEntity, (type) => type.id, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'file_type_id' })
  fileType: FilesTypeEntity;

  @ManyToOne(() => FoldersEntity, (folder) => folder.id, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'folder_id' })
  folder: FoldersEntity;

  @ManyToOne(() => UserEntity, (user) => user.id, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'update_at' })
  updatedAt: Date;
}
