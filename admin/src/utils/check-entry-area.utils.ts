import { CurrentPositionType } from "@/types/current-position.type";
import { ElementPoitionType } from "@/types/element-position.type";

export const checkEntryArea = (el: HTMLDivElement, position: CurrentPositionType): boolean => {
  const pos = el.getBoundingClientRect();
  const positionEl: ElementPoitionType = {
    x: pos.x,
    y: pos.y,
    xw: pos.x + pos.width,
    yh: pos.y + pos.height
  };
  if (
    positionEl.x <= position.x && position.x <= positionEl.xw
    && positionEl.y <= position.y && position.y <= positionEl.yh
  ) {
    return true;
  }
  return false;
}
