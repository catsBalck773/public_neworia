import HTTP from "@/api/HTTP";
import { createFolderType } from "@/types/folders/create-folder.type";
import { HeadersType } from "@/types/headers.type";

export default async (data: createFolderType, headers: HeadersType = {}) =>
  HTTP.post('/file-manager/create-folder', data, {
    headers,
  });
