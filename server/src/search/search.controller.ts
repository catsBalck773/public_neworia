import { Controller, Get, Header, Query } from '@nestjs/common';
import { IResponse } from 'src/interface/IResponse';
import { getResponseSuccess } from 'src/utils/getResponse';
import { SearchService } from './search.service';

interface ISearch {
  q: string;
}

@Controller('search')
export class SearchController {
  constructor(
    private readonly searchServices: SearchService
  ) {}


  @Get()
  @Header('Content-Type', 'application/json')
  async index(@Query() query: ISearch): Promise<IResponse> {
    const result = await this.searchServices.search(query.q);
    return getResponseSuccess([]);
  }
}
