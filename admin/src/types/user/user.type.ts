type GroupType = {
  id: number;
  title: string;
}

export interface UserType {
  id: number;
  login: string;
  email: string;
  gender: string;
  birthday: true,
  isActive: true,
  group: GroupType;
}
