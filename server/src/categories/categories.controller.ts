import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Header,
  Param,
  Query,
  Body,
  HttpCode,
  HttpStatus,
  UseGuards
} from "@nestjs/common";
import { CategoriesService } from "./categories.service";
import { CategoriesDto } from "./dto/categories.dto";
import { JwtAuthGuard } from "src/auth/guards/jwt-auth.guard";
import { IResponse } from "src/interface/IResponse";
import { AuthorGuard } from "src/guards/author.guard";
import { getResponseSuccess } from "src/utils/getResponse";

@Controller('categories')
export class CategoriesController {
  constructor(
    private readonly categoriesService: CategoriesService
  ) { }

  @Get()
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async find(@Query('limit') limit: number, @Query('offset') offset: number): Promise<IResponse> {
    const result = await this.categoriesService.find(limit, offset);
    return getResponseSuccess(result);
  }

  @Get(':id')
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async findOne(@Param('id') id: number): Promise<IResponse> {
    const result = await this.categoriesService.findOne(id);
    return getResponseSuccess(result);
  }

  @Post()
  @UseGuards(JwtAuthGuard)
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.CREATED)
  async created(@Body() params: CategoriesDto): Promise<IResponse> {
    const result = await this.categoriesService.created(params);
    return getResponseSuccess(result);
  }

  @Put(':id')
  @UseGuards(JwtAuthGuard, AuthorGuard)
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async update(@Body() params: CategoriesDto, @Param('id') id: number): Promise<IResponse> {
    const result = await this.categoriesService.update(id, params);
    return getResponseSuccess({ affected: result.affected })
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard, AuthorGuard)
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async remove(@Param('id') id: number): Promise<IResponse> {
    const result = await this.categoriesService.remove(id);
    return getResponseSuccess({ affected: result.affected });
  }
}
