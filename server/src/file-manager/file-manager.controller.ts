import { join } from 'path';

import {
  Body,
  Controller,
  Get,
  Post,
  Delete,
  MaxFileSizeValidator,
  ParseFilePipe,
  Header,
  UploadedFile,
  UseGuards,
  UseInterceptors,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';

import { FileManagerService } from './file-manager.service';
import { IResponse } from 'src/interface/IResponse';
import { fileStorage } from './storage';
import { JwtStrategy } from 'src/auth/strategies/jwt.strategy';
import { GetListDto } from './dto/get-list.dto';
import { ButesFileTypeValidator } from './validators/butes-file-type.validator';
import { allowedFileFormat } from './utils/allowed-file-format';
import { CreateFolderDto } from './dto/create-folder.dto';
import { getResponseSuccess } from 'src/utils/getResponse';
import { MoveFilesInFolderDto } from './dto/move-files-in-folder.dto';
import { userId } from 'src/decorators/user-id.decorator';
import { MoveFoldersInFolderDto } from './dto/move-folders-in-folder.dto';


@Controller('file-manager')
@UseGuards(JwtStrategy)
export class FileManagerController {
  constructor(
    private readonly fileManagerService: FileManagerService
  ) { }

  @Get()
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async getFile(@Body('id') id: string) {
    const result = await this.fileManagerService.getFile(id);
    const path: string = join(process.cwd(), 'uploads', 'ideogram.jpeg');
    return await allowedFileFormat(path);
  }

  @Post('get-list')
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async getList(@Body() payload: GetListDto): Promise<IResponse> {
    const result = await this.fileManagerService.getList(payload);
    return getResponseSuccess(result);
  }

  @Post('upload-file')
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.CREATED)
  @UseInterceptors(FileInterceptor('file', {
    storage: fileStorage,
  }))
  async uploadFile(
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new MaxFileSizeValidator({ maxSize: 14 * 1024 * 1024 }),
          new ButesFileTypeValidator({ fileTypes: ['image/jpeg', 'image/png', 'image/gif'] })
        ]
      })
    ) file: Express.Multer.File,
    @Body('path') path: string,
    @userId() userId: string
  ): Promise<IResponse> {
    const result = await this.fileManagerService.uploadFiles(file, path, userId);
    return getResponseSuccess(result);
  }

  @Delete('remove-file')
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async removeFile(@Body('id') id: string): Promise<IResponse> {
    const result = await this.fileManagerService.removeFile(id);
    return getResponseSuccess(result);
  }

  @Post('move-files-in-folder')
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async moveFilesInFolder(@Body() body: MoveFilesInFolderDto): Promise<IResponse> {
    const result = await this.fileManagerService.moveFilesInFolder(body);
    return getResponseSuccess(result);
  }

  @Post('move-folders-infolder')
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async moveFoldersInFolder(@Body() body: MoveFoldersInFolderDto): Promise<IResponse> {
    const result = await this.fileManagerService.moveFoldersInFolder(body);
    return getResponseSuccess(result);
  }

  @Post('create-folder')
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.CREATED)
  async createFolder(@Body() params: CreateFolderDto, @userId() userId: string): Promise<IResponse> {
    const result = await this.fileManagerService.createFolder(params, userId);
    return getResponseSuccess(result);
  }
}
