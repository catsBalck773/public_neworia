import { Module } from '@nestjs/common';
import { FileManagerController } from './file-manager.controller';
import { FileManagerService } from './file-manager.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FilesEntity } from './entities/files.entity';
import { JwtStrategy } from 'src/auth/strategies/jwt.strategy';
import { FilesTypeEntity } from './entities/files-type.entity';
import { FilesTypeGroupEntity } from './entities/files-type-group.entity';
import { FoldersEntity } from './entities/folders.entity';
import { UserService } from 'src/user/user.service';
import { UserEntity } from 'src/users/entities/user.entity';
import { JwtService } from '@nestjs/jwt';

@Module({
  imports: [
    TypeOrmModule.forFeature([FilesEntity, FilesTypeEntity, FilesTypeGroupEntity, FoldersEntity, UserEntity])
  ],
  controllers: [FileManagerController],
  providers: [FileManagerService, UserService, JwtService, JwtStrategy],
  exports: [FileManagerService]
})
export class FileManagerModule { }
