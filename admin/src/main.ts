import { createApp } from 'vue';
import VueCookies from "vue3-cookies";
import { createPinia } from "pinia";
import createRouter from "@/routes/index";

import App from '@/App.vue';
import '@/assets/styles/index.css';

import { useAppStore } from './stores/app';
import { useAuthStore } from './stores/auth';

const pinia = createPinia();
const router = createRouter();

router.beforeEach((to, from, next) => {
  const authStore = useAuthStore();
  if (!authStore.isCheckAuth) {
    return next();
  }

  const appStore = useAppStore();

  if (authStore.isAuth && to.name === 'Auth') {
    appStore.layoutName = from.meta.layout || 'default';
    return next({ name: from.name });
  }

  appStore.layoutName = to.meta.layout || 'default';  
  if (!authStore.isAuth && to.name !== 'Auth') {
    return next({ name: 'Auth' });
  }
  return next();
});

const app = createApp(App);

app
  .use(VueCookies, {
    expireTimes: "30d",
    path: "/",
    domain: "neworia.ru",
    secure: true
  })
  .use(pinia)
  .use(router);

app.mount('#app');
