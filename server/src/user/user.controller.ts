import { Controller, Delete, Get, Param, Put, UseGuards, Body, Header, HttpCode, HttpStatus } from '@nestjs/common';

import { UserService } from "./user.service";
import { IResponse } from 'src/interface/IResponse';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { AuthorGuard } from 'src/guards/author.guard';
import { AdminGuard } from 'src/guards/admin.guard';
import { getResponseSuccess } from 'src/utils/getResponse';

@Controller('user')
export class UserController {
  constructor(
    private readonly userService: UserService
  ) { }

  @Get(':login')
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async findOne(@Param('login') login: string): Promise<IResponse> {
    const result = await this.userService.findByLogin(login);
    return getResponseSuccess(result);
  }

  @Put(':login')
  @UseGuards(JwtAuthGuard, AuthorGuard, AdminGuard)
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async updated(@Body() payload: any, @Param('login') login: string): Promise<IResponse> {
    const result = await this.userService.update(payload, login)
    return getResponseSuccess(result);
  }

  @Delete(':login')
  @UseGuards(JwtAuthGuard, AdminGuard)
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async remove(@Param('login') login: string): Promise<IResponse> {
    const result = await this.userService.remove(login);
    return getResponseSuccess(result);
  }
}
