export interface LoadingPacksType {
  limit: number;
  offset: number;
}