import { defineAsyncComponent } from "vue";

export default (name: string) => defineAsyncComponent(() => import(`@/layouts/${name}.vue`));
