import { ref } from "vue";
import { FileStoreType } from "@/types/files/file.type";
import { FolderStoreType } from "@/types/files/folder.type";

export const isLock = ref<boolean>(false);
export const isMoved = ref<boolean>(false);

export const currentFile = ref<FileStoreType | null>(null);
export const currentFolder = ref<FolderStoreType | null>(null);

export const stylesMove = ref({ transform: `translate(0px, 0px)` });

const moveElement = (x: number, y: number, options = {
  x: 36,
  y: 36,
}): void => {
  stylesMove.value.transform = `translate(${x - options.x}px, ${y - options.y}px)`;
}

export const useOnMouseElement = () => {

  const onMouseleave = (event: MouseEvent) => {
    if (!isLock.value) return;
    const el = event.target.querySelector('.workspace');
    if (el) {
      isLock.value = false;
      currentFile.value = null;
      currentFolder.value = null;
    }
  }

  const onMousemove = (event: MouseEvent) => {
    if (isLock.value) {
      isMoved.value = true;
    }
    if (isMoved.value) {
      moveElement(event.layerX, event.layerY);
    }
  }

  const onMousedownFile = (file: FileStoreType) => {
    isLock.value = true;
    currentFile.value = file;
  }

  const onMousedownFolder = (folder: FolderStoreType) => {
    isLock.value = true;
    currentFolder.value = folder;
  }

  return {
    onMouseleave,
    onMousedownFile,
    onMousedownFolder,
    onMousemove,
  }
}
