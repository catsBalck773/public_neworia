import { computed, ref } from "vue";
import { defineStore } from "pinia";

export const useAppStore = defineStore('app', () => {
  const isInitApp = ref<boolean>(false);
  const clientWidth = ref<number>(0);
  const clientHeigth = ref<number>(0);

  const isFolding = ref<boolean>(true);
  const isTrigerFolding = ref<boolean>(false);

  const viewPort = computed(() => {
    if (clientWidth.value <= 767) return "mobile";
    else if (768 < clientWidth.value && clientWidth.value <= 1200)
      return "tablet";
    else if (1200 < clientWidth.value && clientWidth.value <= 1535)
      return "laptop";
    return "desktop";
  });

  const layoutName = ref<string>('default');

  return {
    isInitApp,
    clientWidth,
    clientHeigth,
    isFolding,
    isTrigerFolding,
    viewPort,
    layoutName,
  }
});
