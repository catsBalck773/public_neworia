import { ref, computed } from "vue";
import { defineStore } from "pinia";
import { v4 as uuidv4 } from "uuid";

import { Files } from "@/api/index";

import { FileStoreType } from "@/types/files/file.type";
import { FolderStoreType } from "@/types/files/folder.type";
import { createFolderType } from "@/types/folders/create-folder.type";

import { ResponseDataType } from "@/types/response.type";
import { LoadingPacksType } from "@/types/loading-packs.type";
import { ElementEdgesType } from "@/types/element-edges.type";
import { moveElementParams } from "@/types/move-element-params.type";

const getElementEdges = (): ElementEdgesType => ({
  a: 0,
  b: 0,
  c: 0,
  d: 0,
});

export const useFilesStore = defineStore('files', () => {
  const isLoadList = ref<boolean>(false);
  const isUploadFiles = ref<boolean>(false);
  const files = ref<FileStoreType[]>([]);
  const folders = ref<FolderStoreType[]>([]);
  const path = ref<string>('/');

  const getListFilesInFolder = computed(() => [].concat(
    folders.value,
    files.value.map(file => ({
      ...file,
      type: 'file',
    })
    )));

  const isInitStore = ref<boolean>(false);
  const initStore = async (): Promise<void> => {
    isInitStore.value = false;
    await getList('home', { limit: 10, offset: 0 });
    isInitStore.value = true;
  }

  const getList = async (path: string, options: LoadingPacksType): Promise<ResponseDataType | Error> => {
    isLoadList.value = true;
    const result = await Files.getList({
      limit: options.limit,
      offset: options.offset,
      path: path,
    })
      .then(response => {
        if (response.data.success) {
          files.value = response.data.data.files.map(v => {
            v.position = getElementEdges();
            v.isSave = true;
            const file: FileStoreType = JSON.parse(JSON.stringify(v));
            return file;
          });
          folders.value = response.data.data.folders.map(v => {
            v.position = getElementEdges();
            v.isSave = true;
            const folder: FolderStoreType = JSON.parse(JSON.stringify(v));
            return folder;
          });
        }
        return response.data;
      })
      .catch(err => err);

    isLoadList.value = false;
    return result;
  }

  const uploadFiles = async (filesList: FileList, path: string = '/'): Promise<ResponseDataType[] | Error[]> => {
    if (filesList.length === 0) {
      return new Promise(async resolve =>
        resolve(
          [{
            success: false,
            data: [],
            messages: ['Нет выбранных файлов']
          }]
        ));
    }
    isUploadFiles.value = true;
    const results = [];
    for (const file of filesList) {
      const filesUpload = new FormData;
      filesUpload.append('file', file);
      filesUpload.append('path', path);
      const result = await Files.uploadFile(filesUpload, {
        "Content-type": "multipart/form-data",
      })
        .then(response => {
          if (response.data.success) {
            const file = JSON.parse(JSON.stringify(response.data.data));
            file.position = getElementEdges();
            files.value.push(file);
          }
          return response.data;
        })
        .catch(err => err);
      results.push(result);
    }

    isUploadFiles.value = false;

    return results;
  }

  const createFolder = async (params: createFolderType) => {
    const result = await Files.createFolder(params)
      .then(response => {
        if (response.data.success) {
          console.log(response.data)
        }
        return response.data;
      })
      .catch(err => err);

    return result;
  }

  const localCreatedFolder = (parent_id: string, path: string) => {
    const uuid = uuidv4();
    folders.value.unshift({
      id: uuid,
      name: '',
      parent_id: parent_id,
      path: path,
      createdAt: new Date,
      updatedAt: new Date,
      isSave: false,
      position: getElementEdges(),
      files: [],
    });
  }

  const moveFiles = async (params: moveElementParams): Promise<any> => {
    const result = await Files.moveFiles({
      files: params.elems,
      folder_id: params.folder_id,
    })
      .then(response => {
        if (response.data.success && response.data.data.affected) {
          const filesResponse = response.data.data.files;
          files.value = files.value.filter(v => !filesResponse.includes(v.id));
        }
        return response.data;
      })
      .catch(err => err);

    return result;
  }

  const moveFolders = async (params: moveElementParams): Promise<any> => {
    const result = await Files.moveFolders({
      folders: params.elems,
      folder_id: params.folder_id,
    })
      .then(response => {
        if (response.data.success) {
          console.log(response.data)
        }
        return response.data;
      })
      .catch(err => err);

    return result;
  }

  return {
    isInitStore,
    isLoadList,
    isUploadFiles,
    files,
    folders,
    path,
    getListFilesInFolder,
    initStore,
    getList,
    uploadFiles,
    createFolder,
    localCreatedFolder,
    moveFiles,
    moveFolders,
  }
});
