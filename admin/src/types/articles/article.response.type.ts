import { ResponseDataType, ResponseType } from "@/types/response.type";
import { ArticleType } from "./article.type";

interface ArticleData extends ResponseDataType {
  data: {
    article: ArticleType,
  };
}

interface ArticlesData extends ResponseDataType {
  data: {
    articles: ArticleType[],
  };
}

export interface ArticleResponse extends ResponseType {
  data: ArticleData;
}

export interface ArticlesResponse extends ResponseType {
  data: ArticlesData;
}
