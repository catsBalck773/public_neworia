import HTTP from "@/api/HTTP";
import { moveFoldersParams } from "@/types/folders/move-folder-params.type";
import { HeadersType } from "@/types/headers.type";

export default async (data: moveFoldersParams, headers: HeadersType = {}) =>
  HTTP.post('/file-manager/move-folders-in-folder', data, {
    headers,
  });
