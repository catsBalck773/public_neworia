import { Column, Entity, OneToMany, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn } from "typeorm";
import { FilesEntity } from "./files.entity";
import { FilesTypeGroupEntity } from "./files-type-group.entity";

@Entity({ name: 'files_type' })
export class FilesTypeEntity {
  @PrimaryGeneratedColumn({ type: 'int' })
  id: number;

  @Column({ type: 'varchar', length: 99 })
  mime?: string | null;

  @Column({ type: 'varchar', length: 99 })
  extension?: string | null;

  @ManyToOne(() => FilesTypeGroupEntity, (group) => group.id)
  @JoinColumn({ name: 'group_id' })
  groupId: FilesTypeGroupEntity;

  @OneToMany(() => FilesEntity, (file) => file.id)
  files: FilesEntity[];

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'update_at' })
  updatedAt: Date;
}