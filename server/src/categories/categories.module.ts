import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { CategoriesService } from './categories.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CategoriesController } from './categories.controller';
import { CategoryEntity } from './entities/category.entity';
import { JwtStrategy } from 'src/auth/strategies/jwt.strategy';
import { ArticleService } from 'src/article/article.service';
import { ArticleEntity } from 'src/articles/entities/article.entity';
import { UserService } from 'src/user/user.service';
import { UserEntity } from 'src/users/entities/user.entity';
import { ImageEntity } from 'src/images/entities/image.entity';
import { ImagesService } from 'src/images/images.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([CategoryEntity, ArticleEntity, UserEntity, ImageEntity]),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        secret: configService.get('JWT_SECRET'),
        signOptions: { expiresIn: '30d' },
      }),
      inject: [ConfigService]
    })
  ],
  controllers: [CategoriesController],
  providers: [CategoriesService, JwtStrategy, ArticleService, UserService, ImagesService]
})
export class CategoriesModule { }
