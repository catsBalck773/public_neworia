import login from "./login";
import registration from "./registration";
import authCheck from "./auth-check";

export {
  login,
  registration,
  authCheck,
}

export default {
  login,
  registration,
  authCheck,
}