import { IsArray, IsString } from "class-validator";

export class MoveFilesInFolderDto {
  @IsArray()
  files: string[];

  @IsString()
  folder_id: string;
}
