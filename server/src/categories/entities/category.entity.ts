import { Column, CreateDateColumn, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { ArticleEntity } from "src/articles/entities/article.entity";
import { UserEntity } from "src/users/entities/user.entity";

@Entity({ name: 'articles_categories' })
export class CategoryEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 99 })
  name: string;

  @Column({ type: 'varchar', length: 99 })
  title: string;

  @ManyToMany(() => ArticleEntity)
  @JoinTable({ name: 'articles_categories_union' })
  articles: ArticleEntity[];

  @ManyToOne(() => UserEntity, (user) => user.id, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;


  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'update_at' })
  updatedAt: Date;
}
