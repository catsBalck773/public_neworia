import { createRouter, createWebHistory } from "vue-router";
import { IRouteMeta } from "@/interfaces/route-meta.interface";

const routes = [
  {
    name: 'Main',
    path: '/',
    meta: {
      layout: 'default',
    },
    component: () => import('@/pages/Main.vue'),
  },
  {
    name: 'Auth',
    path: '/auth',
    meta: {
      layout: 'auth',
    },
    component: () => import('@/pages/Auth.vue'),
  },
  {
    name: 'Files',
    path: '/files',
    meta: {
      layout: 'default',
    },
    component: () => import('@/pages/Files.vue'),
  },
  {
    name: 'Articles',
    path: '/articles',
    meta: {
      layout: 'default',
    },
    component: () => import('@/pages/Articles.vue'),
  },
  {
    name: 'Users',
    path: '/users',
    meta: {
      layout: 'default',
    },
    component: () => import('@/pages/Users.vue'),
  },
  {
    name: 'Settings',
    path: '/settings',
    meta: {
      layout: 'default',
    },
    component: () => import('@/pages/Settings.vue'),
  },
  {
    name: 'NotFound',
    path: '/:pathMatch(.*)*',
    meta: {
      layout: 'default',
    },
    component: () => import('@/pages/404.vue'),
  },
];

declare module 'vue-router' {
  interface RouteMeta extends IRouteMeta {}
}

export default () => createRouter({
  history: createWebHistory(),
  routes,
});
