import { HeadersType } from "@/types/headers.type";
import HTTP from "../HTTP";

export default async (headers: HeadersType = {}) =>
  HTTP.get('/user/profile', {
    headers,
  });