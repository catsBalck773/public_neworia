import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { ArticleEntity } from "src/articles/entities/article.entity";
import { CategoryEntity } from "src/categories/entities/category.entity";
import { UsersGroupsEntity } from "./users.groups.entity";
import { UserTokenEntity } from "src/users/entities/user-token.entity";
import { FilesEntity } from "src/file-manager/entities/files.entity";
import { FoldersEntity } from "src/file-manager/entities/folders.entity";
import { UserGenderEntity } from "./user-gender.entity";

@Entity('users')
export class UserEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar', length: 25, unique: true })
  login: string;

  @Column({ type: 'varchar', length: 99 })
  password: string;

  @Column({ type: 'varchar', length: 99, unique: true })
  email: string;

  @Column({ type: 'datetime', name: 'birthday', nullable: true })
  birthday: Date | null;

  @Column({ type: 'tinyint', name: 'is_active', default: false })
  isActive: boolean;

  @ManyToOne(() => UsersGroupsEntity, (user) => user.id, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'group_id' })
  group: UsersGroupsEntity;

  @ManyToOne(() => UserGenderEntity, (gender) => gender.id, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'gender_id' })
  gender: UserGenderEntity;

  @OneToMany(() => ArticleEntity, (article) => article.user, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  articles: ArticleEntity[];

  @OneToMany(() => CategoryEntity, (category) => category.user, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  categories: CategoryEntity[];

  @OneToMany(() => FilesEntity, (file) => file.user, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  files: FilesEntity[];

  @OneToMany(() => FoldersEntity, (folder) => folder.user, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  folders: FoldersEntity[];

  @OneToMany(() => UserTokenEntity, (token) => token.user, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  token: UserTokenEntity[];

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'update_at' })
  updatedAt: Date;
}
