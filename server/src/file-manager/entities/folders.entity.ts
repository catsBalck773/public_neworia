import { Entity, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, Column, OneToMany, ManyToOne, JoinColumn } from "typeorm";
import { FilesEntity } from "./files.entity";
import { UserEntity } from "src/users/entities/user.entity";

@Entity({ name: 'folders' })
export class FoldersEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar', length: 99 })
  name: string;

  @Column({ type: 'varchar', length: 99 })
  path: string;
  
  @Column({ type: 'varchar', length: 36, default: '0' })
  parent_id: string;

  @OneToMany(() => FilesEntity, (file) => file.folder, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  files: FilesEntity[];

  @ManyToOne(() => UserEntity, (user) => user.id, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'update_at' })
  updatedAt: Date;
}
