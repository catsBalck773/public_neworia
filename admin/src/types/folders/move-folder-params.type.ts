export type moveFoldersParams = {
  folders: string[],
  folder_id: string,
}
