import HTTP from "@/api/HTTP";
import { HeadersType } from "@/types/headers.type";

export default async (headers: HeadersType = {}) => 
  HTTP.get('/file-manager/get-type-file', {
    headers
  });
