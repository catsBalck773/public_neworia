import { IsString } from "class-validator";

export class CreateFolderDto {
  @IsString()
  parent_id: string;

  @IsString()
  path: string;

  @IsString()
  name: string;
}
