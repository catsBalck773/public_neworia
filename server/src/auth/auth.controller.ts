import { Body, Controller, Header, HttpCode, HttpStatus, Post, UseGuards } from '@nestjs/common';

import { AuthService } from './auth.service';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { UserService } from 'src/user/user.service';
import { CreateUserDto } from 'src/user/dto/create-user.dto';
import { IResponse } from 'src/interface/IResponse';
import { IUser } from 'src/interface/IUser';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { userId } from 'src/decorators/user-id.decorator';
import { getResponseSuccess } from 'src/utils/getResponse';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService
  ) { }

  @Post('login')
  @UseGuards(LocalAuthGuard)
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async login(@Body() payload: IUser): Promise<IResponse> {
    const result = await this.authService.login(payload);
    return getResponseSuccess(result);
  }

  @Post('registration')
  @UseGuards(LocalAuthGuard)
  @HttpCode(HttpStatus.CREATED)
  async registration(@Body() payload: CreateUserDto): Promise<IResponse> {
    const result = await this.userService.created(payload);
    return getResponseSuccess(result);
  }

  @Post('check')
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  async check(@userId() userId: string): Promise<IResponse> {
    const result = await this.authService.check(userId);
    return getResponseSuccess(result);
  }
}
