import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UsersService } from './users.service';
import { UsersController } from './users.controller';

import { UserEntity } from './entities/user.entity';
import { UsersGroupsEntity } from './entities/users.groups.entity';
import { UserService } from 'src/user/user.service';
import { JwtService } from '@nestjs/jwt';
import { AdminGuard } from 'src/guards/admin.guard';

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity, UsersGroupsEntity])],
  providers: [UsersService, UserService, JwtService, AdminGuard],
  controllers: [UsersController]
})
export class UsersModule { }
