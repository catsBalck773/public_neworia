import findOne from "./findOne";
import getProfile from "./getProfile";

export {
  findOne,
  getProfile,
}

export default {
  findOne,
  getProfile,
}
