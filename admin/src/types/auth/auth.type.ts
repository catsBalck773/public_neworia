export interface AuthType {
  // TODO - null убрать как только поправится
  // вывод данных с сервера
  id: number | null,
  login: string,
  group_id?: number;
  token: string;
  time: Date;
}