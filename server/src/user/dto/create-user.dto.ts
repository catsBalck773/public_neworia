import { IsEmail, MaxLength, MinLength, IsNotEmpty, IsString } from "class-validator";

export class CreateUserDto {
  @IsNotEmpty()
  @IsString()
  @MinLength(3, { message: 'Login must ne more then 3 symbols' })
  @MaxLength(25, { message: 'Login should not exceed 25 symbols' })
  readonly login: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(6, { message: 'Password must ne more then 6 symbols' })
  @MaxLength(99, { message: 'Login should not exceed 99 symbols' })
  readonly password: string;

  @IsNotEmpty({message: 'Email не может быть пустым'})
  @IsEmail()
  @MaxLength(99, { message: 'Email should not exceed 99 symbols' })
  readonly email: string;
}
