import { ElementEdgesType } from "../element-edges.type";
import { FileType } from "./file.type";

export interface FolderType {
  id: string;
  name: string;
  path: string;
  parent_id: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface FolderStoreType extends FolderType {
  position: ElementEdgesType;
  files: FileType[],
  isSave: boolean,
}
