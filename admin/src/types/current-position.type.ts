export type CurrentPositionType = {
  x: number,
  y: number,
}
