import { IsInt, IsString } from "class-validator";

export class GetListDto {
  @IsInt()
  limit: number;

  @IsInt()
  offset: number;

  @IsString()
  path: string;
}