export interface IFile {
  mimetype: string;
  size: number;
  encoding: string;
  path: string;
  filename: string;
}
