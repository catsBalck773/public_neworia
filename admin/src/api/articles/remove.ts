import { AxiosResponse } from "axios";
import HTTP from "@/api/HTTP";
import { HeadersType } from "@/types/headers.type";

export default async (id: number, headers: HeadersType = {}): Promise<AxiosResponse> =>
    HTTP.delete(`/article/${id}`, {
        headers,
    });
