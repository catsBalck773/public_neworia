import { MaxLength, MinLength, IsNotEmpty, IsString } from "class-validator";

export class CreateUsersGroupDto {
  @IsNotEmpty()
  @IsString()
  @MinLength(3, { message: 'Login must ne more then 3 symbols' })
  @MaxLength(99, { message: 'Login should not exceed 99 symbols' })
  readonly title: string;
}
