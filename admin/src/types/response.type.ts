import { AxiosResponse } from "axios";

export interface ResponseDataType {
  success: boolean;
  data: any;
  messages: Array<string>
}

export interface IResponse extends AxiosResponse {
  data: ResponseDataType;
}
