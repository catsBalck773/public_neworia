export type ArticleType = {
  id: number;
  title: string;
  description: string;
  createdAt: Date;
  updatedAt: Date;
  datePub: Date;
  content: string;
  categories: Array<any>;
  images: Array<any>;
  user: Array<any>;
  isPublic: boolean;
  isModer: boolean;
  views: number;
}
