import { BadRequestException, Injectable, NotImplementedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { existsSync, mkdir, unlinkSync, renameSync } from 'node:fs';
import { join } from 'node:path';

import { GetListDto } from './dto/get-list.dto';
import { CreateFolderDto } from './dto/create-folder.dto';

import { FilesEntity } from './entities/files.entity';
import { FilesTypeEntity } from './entities/files-type.entity';
import { FoldersEntity } from './entities/folders.entity';
import { MoveFilesInFolderDto } from './dto/move-files-in-folder.dto';
import { UserService } from 'src/user/user.service';
import { MoveFoldersInFolderDto } from './dto/move-folders-in-folder.dto';


type FolderListType = {
  files: FilesEntity[];
  folders: FoldersEntity[];
}

@Injectable()
export class FileManagerService {

  constructor(
    @InjectRepository(FilesEntity)
    private readonly filesRepository: Repository<FilesEntity>,
    @InjectRepository(FilesTypeEntity)
    private readonly filesTypeRepository: Repository<FilesTypeEntity>,
    @InjectRepository(FoldersEntity)
    private readonly foldersRepository: Repository<FoldersEntity>,
    private readonly userService: UserService
  ) { }

  async getList(payload: GetListDto): Promise<FolderListType> {
    const folder = await this.foldersRepository.findOne({
      where: { path: payload.path },
      order: {
        files: {
          createdAt: 'DESC',
        }
      },
      relations: {
        files: {
          fileType: true,
          folder: true,
        },
      },
    });

    if (!folder) {
      return {
        files: [],
        folders: [],
      }
    }

    const folders = await this.getFolders(folder.id, payload.limit, payload.offset);

    return {
      files: folder.files,
      folders: folders,
    }
  }

  async getFile(id: string): Promise<FilesEntity> {
    const file = await this.filesRepository.findOne({
      where: { id }
    });
    if (file) {
      return file;
    }
    return Promise.reject('not file');
  }

  async uploadFiles(file: Express.Multer.File, path: string, userId: string) {
    const fileType = await this.filesTypeRepository.findOne({ where: { mime: file.mimetype } });
    if (!fileType) {
      this.unlinkFile(file);
      throw new BadRequestException("Данный тип файлов не поддерживается");
    }
    const folder = await this.foldersRepository.findOne({ where: { path: path === '/' ? 'home' : path } });
    if (!folder) {
      this.unlinkFile(file);
      throw new BadRequestException("Отсутствует папка для загрузки файлов");
    }
    const user = await this.userService.findById(userId);
    if (!user) {
      this.unlinkFile(file);
      throw new BadRequestException("Не верно переданы данные");
    }
    return await this.filesRepository.save({
      fileName: file.filename,
      size: file.size,
      fileType,
      folder,
      user,
    })
      .then(() => {
        const pathOld: string = join(process.cwd(), file.path);
        const pathNew: string = join(process.cwd(), 'uploads', path, file.filename);
        if (pathOld !== pathNew) {
          renameSync(path, pathNew);
        }
      });
  }

  async removeFile(id: string): Promise<Array<any>> {

    return
  }

  async moveFilesInFolder(body: MoveFilesInFolderDto): Promise<any> {
    const parent = await this.foldersRepository.findOne({ where: { id: body.folder_id } });
    if (!parent) {
      throw new BadRequestException("Вы не верно передали параметр folder_id");
    }

    const files = await this.filesRepository.find({
      where: { id: In(body.files) },
      relations: {
        folder: true,
      }
    });
    for (const file of files) {
      const path: string = join(
        process.cwd(),
        'uploads',
        file.folder.path === 'home' ? '' : file.folder.path,
        file.folder.path === 'home' ? '' : file.folder.name,
        file.fileName
      );
      const pathNew: string = join(
        process.cwd(),
        'uploads',
        parent.path === 'home' ? '' : parent.path,
        parent.name,
        file.fileName
      );
      renameSync(path, pathNew);
    }

    //renameSync()

    const result = await this.filesRepository
      .createQueryBuilder()
      .update(FilesEntity)
      .set({ folder: { id: parent.id } })
      .where({ id: In(body.files) })
      .execute();

    return {
      affected: result.affected,
      files: body.files,
    };
  }

  async createFolder(params: CreateFolderDto, userId: string): Promise<FoldersEntity> {
    const user = await this.userService.findById(userId);
    if (!user) {
      throw new BadRequestException("Вы не можете выполнить это действие");
    }
    const path: string = join(process.cwd(), 'uploads', params.path, params.name);
    const exists = existsSync(path);
    if (!exists) {
      mkdir(path, err => {
        if (err) throw new NotImplementedException("Не удалось создать папку");
      });
      return await this.foldersRepository.save({
        parent_id: params.parent_id,
        path: params.path,
        name: params.name,
        user,
      });
    }
    return await this.foldersRepository.findOne({
      where: {
        path: params.path,
        user,
      }
    });
  }

  async getFolders(parent_id: string, limit: number = 10, offset: number = 0): Promise<FoldersEntity[]> {
    const folders = await this.foldersRepository.find({
      where: { parent_id },
      skip: offset,
      take: limit,
      order: {
        createdAt: 'DESC',
      },
    });
    return folders;
  }

  async updateFolder(): Promise<any> {
    return
  }

  async removeFolder(): Promise<any> {
    return
  }

  async moveFoldersInFolder(body: MoveFoldersInFolderDto): Promise<any> {

    return
  }

  private unlinkFile(file: Express.Multer.File) {
    const path: string = join(process.cwd(), file.path);
    unlinkSync(path);
  }
}
