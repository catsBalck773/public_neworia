import HTTP from "@/api/HTTP";
import { ArticleResponse } from "@/types/articles/article.response.type";
import { HeadersType } from "@/types/headers.type";

export default async (id: number, headers: HeadersType = {}): Promise<ArticleResponse> =>
  HTTP.get(`/article/${id}`, {
    headers,
  });
