import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { UserController } from './user.controller';
import { UserService } from './user.service';

import { UserEntity } from 'src/users/entities/user.entity';
import { JwtStrategy } from 'src/auth/strategies/jwt.strategy';
import { ArticleEntity } from 'src/articles/entities/article.entity';
import { CategoryEntity } from 'src/categories/entities/category.entity';
import { ArticlesService } from 'src/articles/articles.service';
import { CategoriesService } from 'src/categories/categories.service';
import { ArticleService } from 'src/article/article.service';
import { ImagesService } from 'src/images/images.service';
import { ImageEntity } from 'src/images/entities/image.entity';
import { FileManagerService } from 'src/file-manager/file-manager.service';
import { FilesEntity } from 'src/file-manager/entities/files.entity';
import { FilesTypeEntity } from 'src/file-manager/entities/files-type.entity';
import { FoldersEntity } from 'src/file-manager/entities/folders.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      UserEntity,
      CategoryEntity,
      ArticleEntity,
      ImageEntity,
      FilesEntity,
      FilesTypeEntity,
      FoldersEntity,
    ]),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        secret: configService.get('JWT_SECRET'),
        signOptions: { expiresIn: '30d' },
      }),
      inject: [ConfigService]
    })
  ],
  controllers: [UserController],
  providers: [UserService, JwtStrategy, ArticlesService, CategoriesService, ArticleService, ImagesService, FileManagerService],
  exports: [UserService]
})
export class UserModule { }
