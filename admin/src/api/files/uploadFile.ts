import HTTP from "@/api/HTTP";
import { HeadersType } from "@/types/headers.type";

export default async (data: FormData, headers?: HeadersType) =>
  HTTP.post('/file-manager/upload-file', data, headers);