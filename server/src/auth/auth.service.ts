import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as argon2 from "argon2";

import { UserService } from 'src/user/user.service';

import { IUser } from 'src/interface/IUser';
import { UserEntity } from 'src/users/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserTokenEntity } from 'src/users/entities/user-token.entity';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
    @InjectRepository(UserTokenEntity)
    private readonly userTokenRepository: Repository<UserTokenEntity>,
  ) { }

  async validateUser(login: string, password: string): Promise<UserEntity> {
    const user = await this.userService.findByLogin(login);
    if (user) {
      const passwordIsMath = await argon2.verify(user.password, password);
      if (passwordIsMath) {
        return user;
      }
    }
    throw new UnauthorizedException('Неверный логин или пароль');
  }

  async login(user: IUser): Promise<IUser> {
    const my = await this.userService.findByLogin(user.login);
    if (!my) {
      throw new UnauthorizedException('Неверный логин или пароль');
    }

    const payload = {
      id: my.id,
      login: my.login,
      group_id: my.group.id,
    };
    const token = this.jwtService.sign(payload);

    this.userTokenRepository.save({
      token: token,
      user: my,
    });

    return {
      ...payload,
      token: token,
    };
  }

  async check(userId: string): Promise<IUser> {
    const user = await this.userService.findById(userId);

    if (!user) {
      throw new UnauthorizedException('Вы не авторизированы в системе');
    }

    return {
      id: user.id,
      login: user.login,
      group_id: user.group.id,
      token: user.token[0].token,
    }
  }
}
