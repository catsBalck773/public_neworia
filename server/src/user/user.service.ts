import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import * as argon2 from "argon2";

import { UserEntity } from 'src/users/entities/user.entity';
import { CreateUserDto } from './dto/create-user.dto';
import { ICreatedUser } from './types/created-user.type';
import { IUpdatedUser } from './types/updated-user.type';
import { UsersGroupsEntity } from 'src/users/entities/users.groups.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    private readonly jwtService: JwtService
  ) { }

  async findByLogin(login: string): Promise<UserEntity> {
    const result = await this.userRepository.findOne({
      where: {
        login,
      },
      relations: {
        group: true,
        token: true,
        gender: true,
      }
    });

    return result;
  }

  async findById(id: string): Promise<UserEntity> {
    return await this.userRepository.findOne({
      where: {
        id,
      },
      relations: {
        group: true,
        token: true,
      }
    });
  }

  // TODO - вернуться к всему методу
  async created(payload: CreateUserDto): Promise<ICreatedUser> {
    if (!payload.login || !payload.password) {
      throw new BadRequestException('Не все данные заполнены');
    }

    const user = await this.userRepository.findOne({
      where: [
        { login: payload.login },
        { email: payload.email },
      ]
    });
    if (user) {
      const message = user.login === payload.login
        ? `Пользователь с логином "${payload.login}" уже существует`
        : `Пользователь с email "${payload.email}" уже существует`;
      throw new BadRequestException(message);
    }

    const group = new UsersGroupsEntity;
    group.id = 1;

    const res = await this.userRepository.save({
      login: payload.login,
      password: await argon2.hash(payload.password),
      email: payload.email,
      group: group,
      gender: {
        id: 1
      },
    });

    return {
      id: res.id,
      login: res.login,
      email: res.email,
      group_id: res.group.id,
      token: this.jwtService.sign({
        id: res.id,
        login: res.login,
      })
    }
  }

  async update(payload: any, login: string): Promise<IUpdatedUser> {
    const user = await this.findByLogin(login);
    if (!user) {
      throw new BadRequestException(`Пользователя с login'ом "${login}" не существует`)
    }
    if (payload.password) {
      throw new BadRequestException(`Вы не можете менять пароль`);
    }
    const res = await this.userRepository.update(user.id, payload);
    if (res.affected) {
      return {
        id: user.id,
        login: user.login,
        email: user.email,
        group_id: user.group.id,
      }
    }
    throw new BadRequestException(`Не удачная попытка обновления, повторите попытку позже`);
  }

  async remove(login: string): Promise<IUpdatedUser> {
    const user = await this.findByLogin(login);
    if (!user) {
      throw new BadRequestException(`Пользователя с login'ом "${login}" не существует`)
    }

    const res = await this.userRepository.delete(user.id);
    if (res.affected) {
      return {
        id: user.id,
        login: user.login,
        email: user.email,
        group_id: user.group.id,
      }
    }
    throw new BadRequestException(`Не удачная попытка удаления, повторите попытку позже`);
  }
}
