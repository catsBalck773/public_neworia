import { ArrayNotEmpty, ArrayUnique, IsArray, IsOptional, IsString, MaxLength, MinLength } from "class-validator";
import { CategoryEntity } from "src/categories/entities/category.entity";
import { ImageEntity } from "src/images/entities/image.entity";

export class UpdateArticlesDto {
  @IsOptional()
  @MinLength(3)
  @MaxLength(255)
  @IsString()
  readonly title?: string;

  @IsOptional()
  @MinLength(3)
  @IsString()
  readonly description?: string;

  @IsOptional()
  @MinLength(3)
  @IsString()
  readonly content?: string;

  @IsOptional()
  @IsArray()
  @ArrayNotEmpty()
  @ArrayUnique()
  readonly images?: ImageEntity[];

  @IsOptional()
  @IsArray()
  @ArrayNotEmpty()
  @ArrayUnique()
  readonly categories?: CategoryEntity[];
}
