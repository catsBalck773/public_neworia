import { IsNotEmpty, MaxLength, MinLength } from "class-validator";

export class LoginDto {
  @IsNotEmpty()
  @MinLength(3)
  @MaxLength(25)
  login: string;

  @IsNotEmpty()
  @MinLength(6)
  @MaxLength(99)
  password: string;
}