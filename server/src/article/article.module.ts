import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { ArticleService } from './article.service';
import { ArticleController } from './article.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ArticleEntity } from 'src/articles/entities/article.entity';
import { ImageEntity } from 'src/images/entities/image.entity';
import { JwtStrategy } from 'src/auth/strategies/jwt.strategy';
import { ImagesService } from 'src/images/images.service';
import { CategoriesService } from 'src/categories/categories.service';
import { CategoryEntity } from 'src/categories/entities/category.entity';
import { FileManagerModule } from 'src/file-manager/file-manager.module';
import { UserEntity } from 'src/users/entities/user.entity';
import { AuthorGuard } from 'src/guards/author.guard';
import { UserService } from 'src/user/user.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([ArticleEntity, CategoryEntity, UserEntity, ImageEntity]),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        secret: configService.get('JWT_SECRET'),
        signOptions: { expiresIn: '30d' },
      }),
      inject: [ConfigService]
    }),
    FileManagerModule,
  ],
  controllers: [ArticleController],
  providers: [ArticleService, CategoriesService, ImagesService, UserService, JwtStrategy, AuthorGuard],
})
export class ArticleModule { }
