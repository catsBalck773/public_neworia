import HTTP from "@/api/HTTP";
import { AuthRegistrationType } from "@/types/auth/auth.registration.type";
import { AuthResponseType } from "@/types/auth/auth.response.type";
import { HeadersType } from "@/types/headers.type";

export default async (data: AuthRegistrationType, headers: HeadersType = {}): Promise<AuthResponseType> =>
  HTTP.post('/user/reg', data, headers);