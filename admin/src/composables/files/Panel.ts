import { ref } from "vue";

type MenuItemType = {
  key: string;
  name: string;
  action: string;
}

type MenuType = {
  key: string;
  name: string;
  action: string;
  items?: Array<MenuItemType>,
}

type IsMenuType = {
  [key: string]: boolean;
}

export function useMenuPanel() {
  const menu = ref<MenuType[]>([
    {
      key: 'file',
      name: 'Файл',
      action: 'isFileMenuOpen',
      items: [
        {
          key: 'created_folder',
          name: 'Создать папку',
          action: 'createdFolder'
        }
      ]
    },
    {
      key: 'edit',
      name: 'Правка',
      action: 'isEditMenuOpen',
      items: [
        {
          key: 'edit_name',
          name: 'Переименовать',
          action: 'editName',
        }
      ]
    }
  ]);

  const isMenus = ref<IsMenuType>({});

  const initIsMenu = () => menu.value.map(v => isMenus.value[v.action] = false);

  const onMenu = (type: string): void => {
    if (typeof isMenus.value[type] === 'boolean') {
      if (isMenus.value[type] === true) {
        isMenus.value[type] = false;
        return
      }
      Object.keys(isMenus.value).map(k => isMenus.value[k] = false);
      isMenus.value[type] = true;
    }
  }

  return {
    menu,
    isMenus,
    initIsMenu,
    onMenu,
  }
}
