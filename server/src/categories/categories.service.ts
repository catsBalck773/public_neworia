import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { CategoryEntity } from './entities/category.entity';
import { CategoriesDto } from './dto/categories.dto';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectRepository(CategoryEntity)
    private readonly categoryRepository: Repository<CategoryEntity>,
  ) { }

  async find(limit: number = 10, offset: number = 0): Promise<CategoryEntity[]> {
    const categories = await this.categoryRepository.find({
      order: {
        id: 'DESC',
      },
      skip: offset,
      take: limit,
    });

    if (!categories.length) throw new BadRequestException("Список категорий пуст")

    return categories;
  }

  async findOne(id: number): Promise<CategoryEntity> {
    const category = await this.getCategory({ id });
    if (!category) throw new BadRequestException("Нет категории с ID: " + id);
    return category;
  }

  async created(payload: CategoriesDto): Promise<CategoryEntity> {
    const existCategory = await this.getCategory([{ name: payload.name }, { title: payload.title }])
    if (existCategory) throw new BadRequestException('Такая категория уже существует');
    return await this.categoryRepository.save(payload);
  }

  async update(id: number, payload: object) {
    const category = await this.getCategory({ id });
    if (!category) throw new BadRequestException(`Нет категории с ID: ${id}`);
    return await this.categoryRepository.update(id, payload);
  }

  async remove(id: number) {
    const category = await this.getCategory({ id });
    if (!category) throw new BadRequestException(`Нет категории с ID: ${id}`);
    return await this.categoryRepository.delete(id);
  }

  private async getCategory(where: any): Promise<CategoryEntity> {
    return await this.categoryRepository.findOne({ where });
  }
}
