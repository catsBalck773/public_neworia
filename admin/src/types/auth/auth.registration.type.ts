export interface AuthRegistrationType {
  login: string;
  password: string;
  email: string;
}
