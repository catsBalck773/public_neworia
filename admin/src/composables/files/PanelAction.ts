import { useFilesStore } from "@/stores/files";

const filesStore = useFilesStore();

export function usePanelAction() {

  async function createdFolder() {
    filesStore.localCreatedFolder('fecbcb14-30fd-4307-848b-331561260e9b', '/');
  }

  function editName() {

  }

  return {
    createdFolder,
    editName,
  }
}
