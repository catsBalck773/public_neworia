export interface IResponse {
  success: boolean;
  messages: Array<string>;
  data: object;
}