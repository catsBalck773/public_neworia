import { createReadStream } from "fs";
import * as readline from "readline";
import { butesTypeFile } from "../validators/butes-type-file";

type GroupFileType = 'image' | 'document' | '';

export const allowedFileFormat = async (path: string, groupFileType: GroupFileType = ''): Promise<boolean> => {
  const hex = await getHexFile(path, 8);
  const types = Object.entries(groupFileType ? butesTypeFile[groupFileType] : butesTypeFile);
  return allowed(hex, types);
}

function allowed(hex: string, types: any): boolean {
  for (const [key, item] of types) {
    if (typeof item === 'object') {
      for (const k of Object.keys(item)) {
        if (k === hex.slice(0, k.length)) {
          return true;
        }
      }
    } else {
      if (key === hex.slice(0, key.length)) {
        return true;
      }
    }
  }
  return false;
}

export async function getHexFile(path: string, end: number = 3): Promise<string> {
  const readStream = createReadStream(path, { start: 0, end: end, encoding: 'hex' });
  return await new Promise((resolve, reject) => {
    let result;
    const read = readline.createInterface(readStream);
    read.on('line', (input) => {
      result = input;
      read.close();
    });

    read.on('close', () => {
      readStream.close();
      if (result) {
        resolve(result);
      } else {
        reject(false);
      }
    });
  });
}
