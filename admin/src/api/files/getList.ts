import HTTP from "@/api/HTTP";
import { HeadersType } from "@/types/headers.type";
import { LoadingFilesType } from "@/types/loading-files.type";

export default async (params: LoadingFilesType, headers: HeadersType = {}): Promise<any> =>
  HTTP.post('/file-manager/get-list', params, headers);
