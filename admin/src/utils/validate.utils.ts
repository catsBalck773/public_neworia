import { ref } from "vue";

type ValidateType = 'login' | 'password';
type PropType = string | number;

export type ValidateErrorsMesageType = {
  type: string;
  message: string;
}

const validateActions = {
  'login': loginValidate,
  'password': passwordValidate,
}

export const errorsMessage = ref<Array<ValidateErrorsMesageType>>([]);

export function isValidate(prop: PropType, type: ValidateType): boolean | null {
  errorsMessage.value = errorsMessage.value.filter(v => v.type !== type);
  return validateActions[type] ? validateActions[type](prop) : null;
}

function loginValidate(prop: PropType): boolean {
  if (_isEmpty(prop)) {
    errorsMessage.value.push({
      type: 'login',
      message: 'Login не может быть пустым',
    });
    return false;
  }
  if (_isLength(prop, 3)) {
    errorsMessage.value.push({
      type: 'login',
      message: 'Login должен содержать не менее 3 символов',
    })
  }
  if (_isLogin(prop)) {
    errorsMessage.value.push({
      type: 'login',
      message: 'Login может состоять из латинского алфавита и цыфр',
    });
    return false;
  }
  return true;
}

function passwordValidate(prop: PropType): boolean {
  if (_isEmpty(prop)) {
    errorsMessage.value.push({
      type: 'password',
      message: 'Пароль не может быть пустым',
    })
    return false;
  }
  if (_isLength(prop, 6)) {
    errorsMessage.value.push({
      type: 'password',
      message: 'Пароль должен содержать не менее 6 символов',
    });
    return false;
  }
  if (_isPassword(prop)) {
    errorsMessage.value.push({
      type: 'password',
      message: 'Пароль может состоять из латинского и русского алфавита, цыфр и следующих символов(.,-_\'"@?!:$)',
    });
  }
  return true;
}

function _isEmpty(prop: PropType): boolean {
  return !prop;
}

function _isLogin(prop: PropType): boolean {
  return !(/^[a-zA-Z0-9]+$/.test(prop.toString()));
}

function _isPassword(prop: PropType): boolean {
  return !(/^[a-zA-Zа-яА-ЯёЁ0-9\.\,\-\_\'\"\@\?\!\:\$]+$/.test(prop.toString()));
}

function _isLength(prop: PropType, count: number): boolean {
  return String(prop).length < count;
}