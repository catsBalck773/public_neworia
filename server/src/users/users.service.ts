import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { UserEntity } from './entities/user.entity';
import { UsersGroupsEntity } from './entities/users.groups.entity';
import { CreateUsersGroupDto } from "./dto/created-users-group.dto"

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @InjectRepository(UsersGroupsEntity)
    private readonly usersGroupsRepository: Repository<UsersGroupsEntity>
  ) { }

  async findAll(limit: number = 10, offset: number = 0): Promise<UserEntity[]> {
    return await this.userRepository.find({
      select: {
        id: true,
        login: true,
        email: true,
        birthday: true,
        isActive: true,
        group: {
          id: true,
          title: true,
        }
      },
      skip: offset,
      take: limit,
      relations: {
        group: true,
        gender: true,
      }
    });
  }

  async getUsersGroups(limit: number, offset: number): Promise<UsersGroupsEntity[]> {
    return await this.usersGroupsRepository.find({
      skip: offset,
      take: limit,
    });
  }

  async getUsersGroup(id: number): Promise<UsersGroupsEntity> {
    return await this.usersGroupsRepository.findOne({
      where: { id }
    })
  }

  async createdUsersGroup(payload: CreateUsersGroupDto): Promise<UsersGroupsEntity> {
    const group = await this.usersGroupsRepository.findOne({
      where: {
        title: payload.title
      }
    });
    if (group) {
      throw new BadRequestException(`Запись с названием ${payload.title} уже существует`);
    }

    return await this.usersGroupsRepository.save(payload);
  }

  async updateUsersGroup(payload: CreateUsersGroupDto, id: number): Promise<object> {
    const group = await this.usersGroupsRepository.findOne({
      where: [
        { id },
        { title: payload.title }
      ]
    });
    if (!group) {
      const message = payload.title === group.title ? `Значение ${payload.title} уже существует` : `Нет записи с ID ${id}`;
      throw new BadRequestException(message);
    }

    return await this.usersGroupsRepository.update(id, payload);
  }

  async removeUsersGroup(id: number): Promise<object> {
    return await this.usersGroupsRepository.delete(id);
  }
}
