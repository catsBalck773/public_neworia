export type createFolderType = {
  parent_id: string;
  path: string;
  name: string;
}
