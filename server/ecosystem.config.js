module.exports = {
  apps: [
    {
      name: "server",
      script: "npm",
      args: "run start",
      interpreter: 'node'
    }
  ]
}