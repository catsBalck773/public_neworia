module.exports = {
  apps: [
    {
      name: "client",
      script: "npm",
      args: "run preview",
      interpreter: 'node'
    }
  ]
}