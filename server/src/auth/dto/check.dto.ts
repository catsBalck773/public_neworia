import { IsNotEmpty, MaxLength, MinLength } from "class-validator";

export class CheckAuthDto {
  login: string;
  token: string;
}
