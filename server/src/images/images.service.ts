import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { rename, unlink } from "fs";
import { ImageEntity } from './entities/image.entity';

@Injectable()
export class ImagesService {
  private readonly mimetype = {
    'image/jpeg': '.jpg',
    'image/png': '.png',
    'image/gif': '.gif',
  }

  constructor(
    @InjectRepository(ImageEntity)
    private readonly imagesRepository: Repository<ImageEntity>
  ) { }

  async uploadImage(file: Express.Multer.File): Promise<ImageEntity> {
    const filename = `${file.filename}${this.mimetype[file.mimetype]}`;
    rename(file.path, `./uploads/articles/${filename}`, async err => {
      if (err) throw new BadRequestException("Ошибка загрузки файлов");
      return true;
    });

    return await this.imagesRepository.save({
      path: 'uploads/articles',
      name: filename,
    });
  }

  async findOne(id: number): Promise<ImageEntity> {
    return await this.imagesRepository.findOne({ where: { id } });
  }

  async created(image: any): Promise<ImageEntity> {
    return this.imagesRepository.save(image);
  }

  async removeImage(id: number, name: string) {
    await this.imagesRepository.delete(id)
    unlink(`uploads/articles/${name}`, (err) => {
      if (err) throw new BadRequestException("Не удалось удалить файл");
      return true;
    });
  }
}
