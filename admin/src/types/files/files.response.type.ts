import { ResponseDataType, ResponseType } from "@/types/response.type";

interface FileData extends ResponseDataType {
  data: {
    files: any
  }
}

export interface FilesResponseType extends ResponseType {
  data: FileData,
}
