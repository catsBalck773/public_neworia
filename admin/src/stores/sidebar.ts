import { ref } from "vue";
import { defineStore } from "pinia";

export const useSidebarStore = defineStore('sidebar', () => {
  const isOpen = ref<boolean>(true);
  const menuTitle = ref<string>('');

  return {
    isOpen,
    menuTitle,
  }
});
