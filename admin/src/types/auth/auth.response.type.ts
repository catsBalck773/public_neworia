import { ResponseDataType, ResponseType } from "@/types/response.type";
import { AuthType } from "./auth.type";

interface AuthData extends ResponseDataType {
  data: AuthType;
}

export interface AuthResponseType extends ResponseType {
  data: AuthData;
}
