export interface ICreatedUser {
  id: string;
  login: string;
  email: string;
  group_id: number;
  token: string;
}
