import { BadRequestException, FileValidator } from '@nestjs/common';
import { unlink } from "fs";

import { IFile } from '../interfaces/file.interface';
import { butesTypeFile } from './butes-type-file';
import { getHexFile } from '../utils/allowed-file-format';

export type FileTypeValidatorOptions = {
  fileTypes: string[];
};

export class ButesFileTypeValidator extends FileValidator<
  FileTypeValidatorOptions,
  IFile
> {
  buildErrorMessage(): string {
    return `Validation failed (expected type is ${this.validationOptions.fileTypes})`;
  }

  async isValid(file?: IFile) {
    const hex: string = await getHexFile(file.path, 8);
    for (const [key, item] of Object.entries(butesTypeFile.image)) {
      if (this.validationOptions.fileTypes.includes(item) && key === hex.slice(0, key.length)) {
        return true;
      }
    }
    unlink(file.path, (err) => {
      if (err) throw new BadRequestException('err');
    })
    return false;
  }
}
