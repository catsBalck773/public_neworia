import { Column, Entity, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToMany, ManyToOne, JoinColumn, JoinTable } from "typeorm";
import { UserEntity } from "src/users/entities/user.entity";
import { CategoryEntity } from "src/categories/entities/category.entity";
import { ImageEntity } from "src/images/entities/image.entity";

@Entity({ name: 'articles' })
export class ArticleEntity {
  @PrimaryGeneratedColumn('uuid')
  id: number;

  @Column({ type: 'varchar', length: 255 })
  title: string;

  @Column({ type: 'text' })
  description: string;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'update_at' })
  updatedAt: Date;

  @Column({ type: 'datetime', name: 'date_pub', nullable: true })
  datePub: Date;

  @Column({ type: 'mediumtext' })
  content: string;

  @ManyToMany(() => CategoryEntity)
  @JoinTable({ name: 'articles_categories_union' })
  categories: CategoryEntity[];

  @ManyToMany(() => ImageEntity)
  @JoinTable({ name: 'articles_images_union' })
  images: ImageEntity[];

  @ManyToOne(() => UserEntity, (user) => user.id, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;

  @Column({ type: 'tinyint', name: 'is_public', default: false })
  isPublic: boolean;

  @Column({ type: 'tinyint', name: 'is_moder', default: false })
  isModer: boolean;

  @Column({ type: 'bigint', default: 0 })
  views: number;
}
