import { Buffer } from "node:buffer";
import { FolderType } from "./folder.type";
import { ElementEdgesType } from "../element-edges.type";

export interface FileBufferType {
  fieldname: string;
  originalname: string;
  encoding: string;
  mimetype: string;
  buffer: Buffer;
  size: number;
}

export interface FileTypeGroups {
  id: number;
  name: string;
  types?: FileTypes[];
  createdAt: Date;
  updatedAt: Date;
}

export interface FileTypes {
  id: number;
  mime: string;
  extension: string;
  groupId?: FileTypeGroups;
  files?: FileStoreType;
  createdAt: Date;
  updatedAt: Date;
}

export interface FileType {
  id: string;
  fileName: string;
  folder: FolderType;
  size: number;
  fileType: FileTypes;
  createdAt: Date;
  updatedAt: Date;
}

export interface FileStoreType extends FileType {
  position: ElementEdgesType;
  isSave: boolean;
}
