import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

import { ArticleEntity } from 'src/articles/entities/article.entity';
import { CreatedArticleDto } from './dto/created-article.dto';
import { CategoryEntity } from 'src/categories/entities/category.entity';
import { UserEntity } from 'src/users/entities/user.entity';
import { ImageEntity } from 'src/images/entities/image.entity';
import { UpdateArticlesDto } from './dto/update-article.dto';

@Injectable()
export class ArticleService {
  constructor(
    @InjectRepository(ArticleEntity)
    private readonly articleRepository: Repository<ArticleEntity>
  ) { }

  async findOne(id: number): Promise<ArticleEntity> {
    const article = await this.articleRepository.findOne({
      where: { id },
      relations: {
        categories: true,
        images: true,
        user: true,
      }
    });
    if (!article) throw new NotFoundException('Нет записи');
    return article;
  }

  async created(payload: CreatedArticleDto, uid: string): Promise<ArticleEntity> {
    const isArticle = Boolean(await this.getArticle({ title: payload.title }));
    if (isArticle) throw new BadRequestException(`Публикация с url: "${payload.title}" уже существует`);

    try {
      const article = new ArticleEntity();
      article.title = payload.title;
      article.description = payload.description;
      article.images = payload.images.map(i => {
        const image = new ImageEntity();
        image.id = +i;
        return image;
      });
      article.categories = payload.categories.map(c => {
        const category = new CategoryEntity();
        category.id = +c;
        return category;
      });
      article.content = payload.content;

      article.user = new UserEntity;
      article.user.id = uid;

      return await this.articleRepository.save(article);
    } catch (err) {
      throw new BadRequestException("Не верный тип данных");
    }
  }

  async update(payload: UpdateArticlesDto, id: number, uid: string): Promise<any> {
    const article = await this.getArticle({ id });
    if (!article) throw new NotFoundException("Обновляемой записи не существует");
    return await this.articleRepository.update(id, {
      ...payload,
      categories: [{ name: 'test', title: 'test' }],
    });
  }

  async remove(id: number): Promise<any> {
    const article = await this.getArticle({ id });
    if (!article) throw new NotFoundException("Удаляемая запись отсутствует");
    return await this.articleRepository.delete(id);
  }

  private async getArticle(where: any): Promise<ArticleEntity> {
    return await this.articleRepository.findOne({ where });
  }
}
