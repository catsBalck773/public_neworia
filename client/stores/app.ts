interface IAppState {
  isMobile: boolean
  innerWidth: number
}

export const useAppStore = defineStore('app', {
  state: (): IAppState => ({
    isMobile: false,
    innerWidth: 0,
  }),
  getters: {
    viewPort: (state): string => {
      if (state.innerWidth > 1500) {
        return 'desktop';
      }
      if (state.innerWidth <= 1500 && state.innerWidth > 1080) {
        return 'laptop';
      }
      if (state.innerWidth <= 1080 && state.innerWidth > 768) {
        return 'table';
      }
      return 'mobile';
    },
  },
  actions: {
  },
});
