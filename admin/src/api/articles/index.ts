import findAll from "./findAll";
import findOne from "./findOne";
import created from "./created";
import update from "./update";
import remove from "./remove";

export {
  findAll,
  findOne,
  created,
  update,
  remove,
};

export default {
  findAll,
  findOne,
  created,
  update,
  remove,
}