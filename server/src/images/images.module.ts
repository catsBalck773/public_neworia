import { Module } from '@nestjs/common';
import { TypeOrmModule } from "@nestjs/typeorm";
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { ImagesController } from './images.controller';
import { ImageEntity } from "./entities/image.entity";
import { JwtStrategy } from 'src/auth/strategies/jwt.strategy';
import { ImagesService } from './images.service';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { AdminGuard } from 'src/guards/admin.guard';
import { UserService } from 'src/user/user.service';
import { UserEntity } from 'src/users/entities/user.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([ImageEntity, UserEntity]),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        secret: configService.get('JWT_SECRET'),
        signOptions: { expiresIn: '30d' },
      }),
      inject: [ConfigService]
    })
  ],
  controllers: [ImagesController],
  providers: [JwtStrategy, ImagesService, JwtAuthGuard, AdminGuard, UserService]
})
export class ImagesModule { }
