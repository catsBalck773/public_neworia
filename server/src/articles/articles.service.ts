import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { ArticleEntity } from './entities/article.entity';

@Injectable()
export class ArticlesService {
  constructor(
    @InjectRepository(ArticleEntity)
    private readonly articlesRepository: Repository<ArticleEntity>,
  ) { }

  async findAll(limit: number, offset: number): Promise<ArticleEntity[]> {
    const articles = await this.articlesRepository.find({
      skip: offset,
      take: limit,
      relations: {
        categories: true,
        images: true,
        user: true,
      },
      cache: true,
    });

    if (!articles.length) {
      throw new NotFoundException('Нет записи');
    }
    return articles;
  }
}
