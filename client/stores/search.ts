interface ISearchState {
  search: string
  items: Array<object>
}

export const useSearchStore = defineStore('search', {
  state: (): ISearchState => ({
    search: '',
    items: [],
  }),
  getters: {
    
  },
  actions: {
    toSearch(): void {

    },
  },
});
