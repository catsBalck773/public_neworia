import { IResponse } from "src/interface/IResponse";

export const getResponseSuccess = (data: any): IResponse => ({
  success: true,
  data,
  messages: [],
});

export const getResponseError = (messages: string[]): IResponse => ({
  success: false,
  data: [],
  messages,
});
