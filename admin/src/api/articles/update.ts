import { AxiosResponse } from "axios";
import HTTP from "@/api/HTTP";
import { ArticleUpdateType } from "src/types/articles/article.update.type";
import { HeadersType } from "@/types/headers.type";

export default async (id: number, payload: ArticleUpdateType, headers: HeadersType = {}): Promise<AxiosResponse> =>
    HTTP.put(`/article/${id}`, payload, headers);
