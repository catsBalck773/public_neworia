import { Controller, Get, Post, Header, HttpCode, HttpStatus, UseGuards, Body, Param, Put, Delete } from '@nestjs/common';

import { ArticleService } from './article.service';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';

import { IResponse } from 'src/interface/IResponse';
import { CreatedArticleDto } from './dto/created-article.dto';
import { userId } from 'src/decorators/user-id.decorator';
import { getResponseSuccess } from 'src/utils/getResponse';
import { AuthorGuard } from 'src/guards/author.guard';
import { UpdateArticlesDto } from './dto/update-article.dto';

@Controller('article')
export class ArticleController {
  constructor(
    private readonly articleService: ArticleService
  ) { }

  @Get(':id')
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async findOne(@Param('id') id: number): Promise<IResponse> {
    const result = await this.articleService.findOne(id);
    return getResponseSuccess(result);
  }

  @Post('/created')
  @UseGuards(JwtAuthGuard)
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.CREATED)
  async created(@Body() body: CreatedArticleDto, @userId() userId: string): Promise<IResponse> {
    const result = await this.articleService.created(body, userId);
    return getResponseSuccess(result);
  }

  @Put(':type/:id/update')
  @UseGuards(JwtAuthGuard, AuthorGuard)
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async update(@Body() data: UpdateArticlesDto, @Param('id') id: number, @userId() userId: string): Promise<IResponse> {
    const result = await this.articleService.update(data, id, userId)
    return getResponseSuccess({ affected: result.affected });
  }

  @Delete(':type/:id')
  @UseGuards(JwtAuthGuard, AuthorGuard)
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async remove(@Param('id') id: number): Promise<IResponse> {
    const result = await this.articleService.remove(id);
    return getResponseSuccess({ affected: result.affected });
  }
}
