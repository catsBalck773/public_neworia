export interface IMouseEvent extends MouseEvent {
  layerX: number;
  layerY: number;
  target: HTMLElement;
}