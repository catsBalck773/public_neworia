import HTTP from "@/api/HTTP";
import { HeadersType } from "@/types/headers.type";

export default async (id: number, headers: HeadersType = {}): Promise<object> =>
  HTTP.get(`/user/${id}`, {
    headers,
  });