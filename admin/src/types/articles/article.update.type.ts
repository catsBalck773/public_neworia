export type ArticleUpdateType = {
  title?: string;
  description?: string;
  content?: string;
  images?: Array<number>;
  categoories?: Array<number>;
}
