import { ref } from "vue";
import { defineStore } from "pinia";
import { User } from "@/api/index";
import { UserType } from "@/types/user/user.type";
import { ResponseDataType } from "@/types/response.type";

export const useUserStore = defineStore('user', () => {
  const user = ref<UserType>();

  const getProfile = async (): Promise<ResponseDataType | Error> => {
    const result = await User.getProfile()
      .then(response => {
        return response.data;
      })
      .catch(err => err)

    return result;
  }

  return {
    user,
    getProfile,
  }
});
