import { Body, Controller, Delete, Get, Header, HttpCode, HttpStatus, Param, Post, Put, Query, UseGuards } from '@nestjs/common';

import { UsersService } from './users.service';
import { IResponse } from 'src/interface/IResponse';
import { CreateUsersGroupDto } from './dto/created-users-group.dto';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { AdminGuard } from 'src/guards/admin.guard';
import { getResponseSuccess } from 'src/utils/getResponse';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) { }

  @Get()
  @UseGuards(JwtAuthGuard)
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async findAll(@Query('limit') limit: number, @Query('offset') offset: number): Promise<IResponse> {
    const result = await this.usersService.findAll(limit, offset);
    return getResponseSuccess(result);
  }

  @Get('groups')
  @UseGuards(JwtAuthGuard, AdminGuard)
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async getUsersGroups(@Query('limit') limit: number, @Query('offset') offset: number): Promise<IResponse> {
    const result = await this.usersService.getUsersGroups(limit, offset);
    return getResponseSuccess(result);
  }

  @Get('group/:id')
  @UseGuards(JwtAuthGuard, AdminGuard)
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async getUsersGroup(@Param('id') id: number): Promise<IResponse> {
    const result = await this.usersService.getUsersGroup(id);
    return getResponseSuccess(result);
  }

  @Post('group/create')
  @UseGuards(JwtAuthGuard, AdminGuard)
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.CREATED)
  async createdUsersGroup(@Body() payload: CreateUsersGroupDto): Promise<IResponse> {
    const result = await this.usersService.createdUsersGroup(payload);
    return getResponseSuccess(result);
  }

  @Put('group/:id')
  @UseGuards(JwtAuthGuard, AdminGuard)
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async updateUsersGroup(@Body() payload: CreateUsersGroupDto, @Param('id') id: number): Promise<IResponse> {
    const result = await this.usersService.updateUsersGroup(payload, id);
    return getResponseSuccess(result);
  }

  @Delete('group/:id')
  @UseGuards(JwtAuthGuard, AdminGuard)
  @Header('Content-Type', 'application/json')
  @HttpCode(HttpStatus.OK)
  async removeUsersGroup(@Param('id') id: number): Promise<IResponse> {
    const result = await this.usersService.removeUsersGroup(id);
    return getResponseSuccess(result);
  }
}
