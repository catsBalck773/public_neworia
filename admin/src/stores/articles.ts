import { ref } from "vue";
import { defineStore } from "pinia";

import { Articles } from "@/api";
import { ArticleType } from "src/types/articles/article.type";
import { ArticleCreatedType } from "src/types/articles/article.created.type";
import { ResponseDataType } from "@/types/response.type";

export const useArticlesStore = defineStore('articles', () => {
  const articles = ref<ArticleType[]>();

  const getArticles = async (limit: number, offset: number): Promise<ArticleType[] | Error> => {
    return await Articles.findAll({ limit, offset })
      .then(response => {
        return response.data.data.articles;
      })
      .catch(err => err);
  }

  const getArticle = async (id: number): Promise<ArticleType | void> => {
    return await Articles.findOne(id)
      .then(response => response.data.data.article)
      .catch(err => err);
  }

  const createdArticle = async (payload: ArticleCreatedType): Promise<ArticleType | Error> => {
    const formData = new FormData;
    for (const key of Object.keys(payload)) {
      const value = payload[key];
      formData.append(key, value);
    }
    return await Articles.created(formData)
      .then(response => {
        return response.data.data.article;
      })
      .catch(err => err);
  }

  const updateArticle = async (id: number, payload): Promise<ResponseDataType | Error> => {
    return await Articles.update(id, payload)
      .then(response => response.data)
      .catch(err => err)
  }

  const removeArticle = async (id: number): Promise<ResponseDataType | Error> => {
    return await Articles.remove(id)
      .then(res => res.data)
      .catch(err => err);
  }

  return {
    articles,
    getArticles,
    getArticle,
    createdArticle,
    updateArticle,
    removeArticle,
  }
});
