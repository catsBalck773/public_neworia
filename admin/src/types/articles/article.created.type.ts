export type ArticleCreatedType = {
  title: string;
  description: string;
  content: string;
  images?: Array<number>;
  categoories: Array<number>;
}
