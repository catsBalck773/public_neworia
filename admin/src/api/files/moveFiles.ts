import HTTP from "@/api/HTTP";
import { moveFilesParams } from "@/types/files/move-files-params.type";
import { HeadersType } from "@/types/headers.type";
import { ResponseDataType } from "@/types/response.type";
import { AxiosResponse } from "axios";

interface ResponseDataMoveFile extends ResponseDataType {
  data: {
    affected: number;
    files: string[];
  }
}

interface ResponseMoveFile extends AxiosResponse {
  data: ResponseDataMoveFile;
}

export default async (data: moveFilesParams, headers: HeadersType = {}): Promise<ResponseMoveFile> =>
  HTTP.post('/file-manager/move-files-in-folder', data, {
    headers,
  });
