import HTTP from "@/api/HTTP";
import { FilesResponseType } from "@/types/files/files.response.type";
import { HeadersType } from "@/types/headers.type";
import { LoadingPacksType } from "@/types/loading-packs.type";

export default async (params: LoadingPacksType, headers: HeadersType = {}): Promise<FilesResponseType> =>
  HTTP.get('/files/', {
    params,
    headers,
  });
