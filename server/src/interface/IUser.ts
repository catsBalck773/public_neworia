export interface IUser {
  id: string;
  login: string;
  group_id: number;
  token?: string;
}
