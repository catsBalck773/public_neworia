export type AuthLoginType = {
  login: string;
  password: string;
}
