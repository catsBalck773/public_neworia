import { Module } from '@nestjs/common';
import { TypeOrmModule } from "@nestjs/typeorm";
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { ArticlesService } from './articles.service';
import { ArticlesController } from './articles.controller';
import { ArticleEntity } from "./entities/article.entity";
import { ArticleService } from 'src/article/article.service';
import { CategoryEntity } from 'src/categories/entities/category.entity';
import { CategoriesService } from 'src/categories/categories.service';
import { UserEntity } from 'src/users/entities/user.entity';
import { UserService } from 'src/user/user.service';
import { JwtStrategy } from 'src/auth/strategies/jwt.strategy';
import { ImagesService } from 'src/images/images.service';
import { ImageEntity } from 'src/images/entities/image.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([ArticleEntity, CategoryEntity, UserEntity, ImageEntity]),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        secret: configService.get('JWT_SECRET'),
        signOptions: { expiresIn: '30d' },
      }),
      inject: [ConfigService]
    })
  ],
  controllers: [ArticlesController],
  providers: [ArticlesService, ArticleService, CategoriesService, JwtStrategy, ImagesService],
})
export class ArticlesModule { }
